// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "PinchSDK",
    products: [
        .library(
            name: "PinchSDK",
            targets: ["PinchSDK"]
        )
    ],
    dependencies: [
        .package(
            url:"https://gitlab.com/fluxloop/swift/pinchsdklegacy.git",
            .exact("1.0.103")
        ),
        .package(
            url:"https://gitlab.com/fluxloop/swift/pinchsdklegacybluetooth.git",
            .exact("1.0.103")
        )
    ],
    targets: [
        .binaryTarget(
            name: "PinchSDK",
            url: "https://puresdk.blob.core.windows.net/pinchsdk-versions/xcf/2.2.0axcf.zip",
            checksum: "d6d0eab9c3aacb47a504703f61b064c2d38b5dd651913669358a683d8e1d472c"
        )
    ]
)
